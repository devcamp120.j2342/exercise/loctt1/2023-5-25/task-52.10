import model.person;

public class App {
    public static void main(String[] args) throws Exception {
        person person1 = new person();

        person person2 = new person("Devcamp", "User", "Male", "devcamp@gamil.com", "devcamp@gmail.com");
        System.out.println(person1.toString());
        System.out.println(person2.toString());

        System.out.println("-----------------------");
        System.out.println(person1.getFirstName());
        System.out.println(person2.getFirstName());

        System.out.println("-----------------------");
        person1.setFirstName("update");
        System.out.println(person1.getFirstName());
    }
}
